package com.example.seconduzd

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.seconduzd.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), IMainAct {

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var mainBinding: ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        val viewModel : ViewModel = ViewModel(this)

        mainBinding.viewmodel =  viewModel
        val adapter: RecyclerVAdapter = RecyclerVAdapter(mutableListOf(), layoutInflater)

        mainBinding.recyclerView.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager
        mainBinding.recyclerView.adapter = adapter

        viewModel.repoLiveData.observe(this, Observer{ jobs ->
            adapter.renewList(jobs)
        })
        setContentView(mainBinding.root)
    }

    override fun openCreationScreen (){

        val intent = Intent(this, AddActivity::class.java)
        startActivity(intent)
    }
}
