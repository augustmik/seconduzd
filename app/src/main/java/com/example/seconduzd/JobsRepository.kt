package com.example.seconduzd

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

object JobsRepository {
    private var list: MutableList<Job> = mutableListOf()
    private val _jobMutableList: MutableLiveData<MutableList<Job>> = MutableLiveData()

    fun getJobs() = _jobMutableList

    init {
        AddExtraJob("naujas", "darbas")
    }

    fun AddExtraJob(title: String, message: String) {
        list.add(Job(title, message))
        _jobMutableList.postValue(list)

    }
}