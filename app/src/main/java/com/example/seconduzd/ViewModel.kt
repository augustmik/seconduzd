package com.example.seconduzd

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData

class ViewModel (var iMainAct: IMainAct)  {

    val repoLiveData : MutableLiveData<MutableList<Job>> = JobsRepository.getJobs()

    fun addNewJobClicked() {
        iMainAct.openCreationScreen()
    }


}