package com.example.seconduzd

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.seconduzd.databinding.ActivityAddBinding
import com.example.seconduzd.databinding.ActivityMainBinding

import kotlinx.android.synthetic.main.activity_add.*

class AddActivity : AppCompatActivity() {

    private val jobRepo = JobsRepository

    lateinit var binding: ActivityAddBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)

    }

    fun ButtonClicked(view: View) {
        //binding = ActivityAddBinding.inflate()//.inflate(layoutInflater)

        Log.i("TitleText", TitleTV.text.toString())
        Log.i("MessageText", MessageTV.text.toString())
        //val field2 = findViewById(R.id.MessageTV) as EditText
        //jobRepo.AddExtraJob(binding.TitleTV.text.toString(), field2.text.toString()/*binding.MessageTV.text.toString()*/) //both dont update
        jobRepo.AddExtraJob(TitleTV.text.toString(), MessageTV.text.toString())
        finish()
    }

}
