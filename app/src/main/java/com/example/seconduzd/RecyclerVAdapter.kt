package com.example.seconduzd

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.seconduzd.databinding.ItemInRecyclerBinding

class RecyclerVAdapter(
    private var myDataset: MutableList<Job>,
    private val layoutInflater: LayoutInflater
) : RecyclerView.Adapter<RecyclerVAdapter.MyViewHolder>() {

    class MyViewHolder(val binding: ItemInRecyclerBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(todo: Job) {
            binding.textView.text = todo.name
            binding.textView2.text = todo.aboutJ
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(myDataset.get(position))
    }

    override fun getItemCount() = myDataset.size

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val textView = DataBindingUtil.inflate<ItemInRecyclerBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_in_recycler,
            parent,
            false
        )
        return MyViewHolder(textView)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun renewList(items: MutableList<Job>) {
        myDataset.clear()
        myDataset.addAll(items)
        notifyDataSetChanged()
    }
}